from timeit import default_timer as timer
# https://adventofcode.com/2022/day/5

start = timer()


def main():
    with open('input.txt', 'r') as f:
        content = f.read().splitlines()

    # "crates_rows" is a list of each line from the input section that represents the crates
    crates_rows = content[:content.index("")-1]
    # "stacks" is the integer of the last (highest) number from the stack number row
    stacks = int(content[content.index("") - 1].split(' ')[-2])
    # "instructions" is a list of each line from the input section that represents the instructions
    instructions = content[content.index("")+1:]

    crate_stacks = []  # Will become a list of strings representing stacks, ordered from top crate to bottom

    for stack_number in range(stacks):  # Looping through each stack one at a time (not very efficient)
        crate_stack = ''
        for j in crates_rows:  # Looping through each crate row
            try:
                if not j[(stack_number*4)+1] == ' ':
                    crate_stack += j[(stack_number*4)+1]  # Starting at index 1, crates are every 4th character
            except IndexError:  # First several rows might be shorter length, no problem
                continue

        crate_stacks.append(crate_stack)

    for instruction in instructions:
        move_qty = int(instruction.split(' ')[1])
        source_stack = int(instruction.split(' ')[3]) - 1  # The index of the column is one less than the column num
        dest_stack = int(instruction.split(' ')[5]) - 1

        for _ in (range(move_qty)):

            # Add the first value of the source stack to the destination stack, one at a time
            crate_stacks[dest_stack] = crate_stacks[source_stack][0] + crate_stacks[dest_stack]

            # Remove the first value from the source stack
            crate_stacks[source_stack] = crate_stacks[source_stack][1:]

    print(''.join([x[0] for x in crate_stacks]))


main()

end = timer()

print(f"Time elapsed: {end - start}")
