from timeit import default_timer as timer
# https://adventofcode.com/2022/day/2

start = timer()


def main():
    with open('input.txt', 'r') as f:
        strategy_guide = f.read().splitlines()

    # POINTS

    # X (Rock) = 1
    # Y (Paper) = 2
    # Z (Scissors) = 3

    # Loss = 0
    # Draw = 3
    # Win = 6

    # A (Rock) loses to Y (Paper)
    # B (Paper) loses to Z (Scissors)
    # C (Scissors) beats X (Rock)

    total_score = 0

    scenarios = {  # Key: opponents choice, Value: Our choice's point value, ordered by our Loss, Draw, Win
        'A': [3, 1, 2],  # A (Rock) wins against Scissors, draws against Rock, loses to Paper
        'B': [1, 2, 3],  # B (Paper) wins against Rock, draws against Paper, loses to Scissors
        'C': [2, 3, 1]   # C (Scissors) wins against Paper, draws against Scissors, loses to Rock
    }

    win_scenarios = ['A Y', 'B Z', 'C X']
    draw_scenarios = ['A X', 'B Y', 'C Z']

    for round in strategy_guide:
        if round[2] == 'X':  # Need to lose
            total_score += scenarios[round[0]][0]

        elif round[2] == 'Y':  # Need to draw
            total_score += 3 + scenarios[round[0]][1]

        else:  # Need to win
            total_score += 6 + scenarios[round[0]][2]

    print(total_score)


main()

end = timer()

print(f"Time elapsed: {end - start}")
