from timeit import default_timer as timer
# https://adventofcode.com/2022/day/2

start = timer()


def main():
    with open('input.txt', 'r') as f:
        strategy_guide = f.read().splitlines()

    # POINTS

    # X (Rock) = 1
    # Y (Paper) = 2
    # Z (Scissors) = 3

    # Loss = 0
    # Draw = 3
    # Win = 6

    # A (Rock) loses to Y (Paper)
    # B (Paper) loses to Z (Scissors)
    # C (Scissors) beats X (Rock)

    total_score = 0

    score_dict = {
        'X': 1,
        'Y': 2,
        'Z': 3
    }

    win_scenarios = ['A Y', 'B Z', 'C X']
    draw_scenarios = ['A X', 'B Y', 'C Z']

    for round in strategy_guide:
        if round in draw_scenarios:  # Draw
            total_score += score_dict[round[2]] + 3

        elif round in win_scenarios:  # Win
            total_score += score_dict[round[2]] + 6

        else:  # Lose
            total_score += score_dict[round[2]]

    print(total_score)


main()

end = timer()

print(f"Time elapsed: {end - start}")
