from timeit import default_timer as timer
# https://adventofcode.com/2022/day/1

start = timer()


def main():
    with open('input.txt', 'r') as f:
        cal_list = f.read().splitlines()

    calorie_sums = []  # List to store the calorie sums per elf
    calorie_total = 0

    for value in cal_list:
        if not value:  # A blank value indicates the end of one elf's calorie total
            calorie_sums.append(calorie_total)
            calorie_total = 0
        else:
            calorie_total += int(value)

    print(max(calorie_sums))


main()

end = timer()

print(f"Time elapsed: {end - start}")
