from timeit import default_timer as timer
# https://adventofcode.com/2022/day/3

start = timer()


def main():
    with open('input.txt', 'r') as f:
        rucksack_list = f.read().splitlines()

    score = 0

    for i in range(0, len(rucksack_list), 3):
        print(i)

        # Create sets from each rucksack in the group of 3, find intersecting character
        comp_1 = set(rucksack_list[i])
        comp_2 = set(rucksack_list[i + 1])
        comp_3 = set(rucksack_list[i + 2])
        shared_item = str(comp_1.intersection(comp_2, comp_3))[2]

        # Add to the score based on the Unicode code
        if shared_item.isupper():
            score += ord(shared_item) - 38  # ord('A') = 65, for scoring should be 27
        if shared_item.islower():
            score += ord(shared_item) - 96  # ord('a') = 97, for scoring should be 1

    print(score)


main()

end = timer()

print(f"Time elapsed: {end - start}")
