from timeit import default_timer as timer
# https://adventofcode.com/2022/day/4

start = timer()


def main():
    with open('input.txt', 'r') as f:
        assignments = f.read().splitlines()

    pair_count = 0  # Count of pairs where one fully contains the other

    for assignment_pair in assignments:

        # Converts the elf range pair in each line to a pair of integer tuples
        elf1, elf2 = assignment_pair.split(',')
        elf1 = (int(elf1.split('-')[0]), int(elf1.split('-')[1]))
        elf2 = (int(elf2.split('-')[0]), int(elf2.split('-')[1]))

        if (elf1[0] >= elf2[0]) and (elf1[1] <= elf2[1]):  # If elf1 is contained within elf2's range
            pair_count += 1
        elif (elf2[0] >= elf1[0]) and (elf2[1] <= elf1[1]):  # If elf2 is contained within elf1's range
            pair_count += 1

    print(pair_count)


main()

end = timer()

print(f"Time elapsed: {end - start}")
