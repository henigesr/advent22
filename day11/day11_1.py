from timeit import default_timer as timer

# https://adventofcode.com/2022/day/11

start = timer()


def notes_parser():

    # Returns a dictionary of single key: value pairs, where the value is the attributes for a monkey
    # The key is the integer id of the monkey, the value dictionary contains this information-
    #   items: A list of the items (worry level)
    #   op: (string) How the worry level changes upon inspection
    #   test: Integer to divide an item by to see if it should be thrown
    #   true: Integer id of the monkey to throw to if the test is true
    #   false: Integer id of the monkey to throw to if the test is false
    #   business: Tracker for how many times each monkey inspects items

    with open('input.txt', 'r') as f:
        notes = f.read().splitlines()

    notes_dict = {}
    monkey_dict = {'business': 0}

    for note in notes:

        attribute = note.strip().split(' ')[0]

        if attribute == "Monkey":
            monkey_num = int(note.strip().split(' ')[1].replace(':', ''))

        if attribute == "Starting":
            monkey_dict.update({"items": [int(x) for x in note.split(':')[1].replace(' ', "").split(',')]})

        if attribute == "Operation:":
            monkey_dict.update({"op": note.split('=')[1].strip()})

        if attribute == "Test:":
            monkey_dict.update({"test": int(note.split(' ')[-1])})

        if attribute == "If":
            if note.strip().split(' ')[1] == "true:":
                monkey_dict.update({"true": int(note.split(' ')[-1])})

            if note.strip().split(' ')[1] == "false:":
                monkey_dict.update({"false": int(note.split(' ')[-1])})

        if note == '':
            notes_dict.update({monkey_num: monkey_dict})
            monkey_dict = {'business': 0}

    notes_dict.update({monkey_num: monkey_dict})

    return notes_dict


def worry_level_adjustment(item, op):

    val1, operation, val2 = op.split(' ')

    if val1 == "old":
        val1 = item
    else:
        val1 = int(val1)

    if val2 == "old":
        val2 = item
    else:
        val2 = int(val2)

    if operation == '+':
        new_item = val1 + val2

    if operation == '*':
        new_item = val1 * val2

    return int(new_item / 3)


def main():

    # A dictionary of dictionaries ({monkey: monkey attributes})
    notes = notes_parser()

    # 20 rounds for part 1 of the challenge
    for _ in range(20):
        # Loop through each monkey's item, one item at a time
        for monkey in notes:
            for index in range(len(notes[monkey]['items'])):

                # The monkey will throw the item, so remove it from that monkey's items
                item = notes[monkey]['items'].pop(0)
                # Adjust the item's worry level
                item = worry_level_adjustment(item, notes[monkey]['op'])
                # Track "monkey business" or how many times each monkey has held an item
                notes[monkey]['business'] += 1

                if item % notes[monkey]['test'] == 0:
                    # Throw to 'true' monkey
                    notes[notes[monkey]['true']]['items'].append(item)
                else:
                    # Throw to 'false' monkey
                    notes[notes[monkey]['false']]['items'].append(item)

    # Find the product of the highest two "monkey business" values
    sorted_monkey_business = sorted([_['business'] for _ in notes.values()])
    print(sorted_monkey_business[-1] * sorted_monkey_business[-2])


main()

end = timer()

print(f"Time elapsed: {end - start}")
