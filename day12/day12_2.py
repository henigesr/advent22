from timeit import default_timer as timer

# https://adventofcode.com/2022/day/12

start = timer()


def main():
    # https://www.hackerearth.com/practice/algorithms/graphs/breadth-first-search/tutorial/

    # Generate grid
    # You can determine the height at x, y, with 0, 0 at the top left
    # where x determines the row index, and y determines the column index
    with open('input.txt', 'r') as f:
        grid = [[*_] for _ in f.read().splitlines()]

    # Find the coordinates of the start and end
    end_coordinates = [(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == "E"][0]
    s_coordinates = [(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == "S"][0]

    # And replace them with their height equivalents
    grid[s_coordinates[0]][s_coordinates[1]] = 'a'
    grid[end_coordinates[0]][end_coordinates[1]] = 'z'

    borders = [(0, 1), (0, -1), (1, 0), (-1, 0)]

    # Make a list of coordinates for all 'a' locations
    a_coordinates = [(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == "a"]

    # A list to store the distance from all 'a' coordinates to the end
    a_coordinate_lengths = []

    # Run the code for part 1 against all 'a' locations
    for start_coordinates in a_coordinates:
        # Keep a queue of coordinates that we'll go through in a first in/first out manner
        queue = [start_coordinates]

        # Track coordinates that have been explored, and the steps from the start for that coordinate
        explored = {start_coordinates: 0}

        while queue:

            # Remove oldest coordinate from queue
            v = queue.pop(0)

            if v == end_coordinates:
                a_coordinate_lengths.append(explored[v])
                break

            # Add all borders that can be reached and haven't been explored to the queue
            for bx, by in borders:
                bx += v[0]
                by += v[1]

                if bx in range(len(grid)) and by in range(len(grid[0])):
                    if ord(grid[v[0]][v[1]]) >= ord(grid[bx][by])-1:
                        if (bx, by) not in explored:
                            explored.update({(bx, by): explored[v] + 1})
                            queue.append((bx, by))

    # Print the shortest distance between any 'a' and the end
    print(sorted(a_coordinate_lengths)[0])


main()

end = timer()

print(f"Time elapsed: {end - start}")  # Time elapsed: 0.9184932
