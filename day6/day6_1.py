from timeit import default_timer as timer
# https://adventofcode.com/2022/day/6

start = timer()


def main():
    with open('input.txt', 'r') as f:
        signal = f.read()

    for i in range(0, len(signal)):
        if len(set(signal[i:i+4])) == 4:
            print(i+4)
            break


main()

end = timer()

print(f"Time elapsed: {end - start}")
