from timeit import default_timer as timer

# https://adventofcode.com/2022/day/9

start = timer()


def main():
    with open('input.txt', 'r') as f:
        instructions = f.read().splitlines()

    # Each list of length 2 is the x, y coordinates of a knot in the rope
    rope = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]

    # The change in position of the head based on the direction of the move
    pos_delta = {
        'R': [1, 0],
        'L': [-1, 0],
        'U': [0, 1],
        'D': [0, -1]
    }

    # Tracking history of the tail position in a set, tail starts at (0, 0)
    tail_pos_history = {(0, 0)}

    for instruction in instructions:
        direction, steps = instruction.split(' ')

        for step in range(0, int(steps)):

            for index in range(0, len(rope) - 1):

                if index == 0:  # Only the head moves based on the directions
                    rope[0] = [x + y for x, y in zip(rope[0], pos_delta[direction])]

                # Tracks the delta in x and y between the current knot and the subsequent one
                head_tail_dist = [x - y for x, y in zip(rope[index], rope[index + 1])]

                # Pythagorean theorem, subsequent knot needs to move if it's 2 "units" away or greater
                # than the current knot
                if (head_tail_dist[0] ** 2 + head_tail_dist[1] ** 2) >= 4:
                    # Move the subsequent knot +/- 1 unit horizontally and/or vertically
                    if not head_tail_dist[0] == 0:
                        rope[index + 1][0] += int(head_tail_dist[0] / abs(head_tail_dist[0]))
                    if not head_tail_dist[1] == 0:
                        rope[index + 1][1] += int(head_tail_dist[1] / abs(head_tail_dist[1]))

            # Maintain the tail position history set for the answer
            tail_pos_history.add((rope[-1][0], rope[-1][1]))

    print(len(tail_pos_history))


main()

end = timer()

print(f"Time elapsed: {end - start}")
