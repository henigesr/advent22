from timeit import default_timer as timer

# https://adventofcode.com/2022/day/9

start = timer()


def main():
    with open('input.txt', 'r') as f:
        instructions = f.read().splitlines()

    # Will track position using tuples representing x and y axis positions
    head_pos = (0, 0)
    tail_pos = (0, 0)

    # The change in position of the head based on the direction of the move
    pos_delta = {
        'R': (1, 0),
        'L': (-1, 0),
        'U': (0, 1),
        'D': (0, -1)
    }

    # Tracking history of the tail position in a set, tail starts at (0, 0)
    tail_pos_history = {(0, 0)}

    for instruction in instructions:
        direction, steps = instruction.split(' ')

        for step in range(0, int(steps)):
            # Recording the heads previous position, as that may become the tail's new position
            head_pos_prev = head_pos

            # https://stackoverflow.com/questions/497885/python-element-wise-tuple-operations-like-sum
            head_pos = tuple(map(sum, zip(head_pos, pos_delta[direction])))

            head_tail_dist = tuple(map(lambda x, y: x - y, head_pos, tail_pos))

            # Pythagorean theorem, tail needs to move if it's 2 "units" away or greater
            if (head_tail_dist[0] ** 2 + head_tail_dist[1] ** 2) >= 4:
                tail_pos = head_pos_prev

            # Maintain the tail position history for the answer
            tail_pos_history.add(tail_pos)

    print(len(tail_pos_history))


main()

end = timer()

print(f"Time elapsed: {end - start}")
