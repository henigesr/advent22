from timeit import default_timer as timer

# https://adventofcode.com/2022/day/10

start = timer()


def main():
    with open('input.txt', 'r') as f:
        instructions = f.read().splitlines()

    cycles = 0  # Tracks what cycle we're on
    cycle_check = 20  # The next cycle to find the signal strength of
    register = 1  # The CPU register
    signal_strength_sum = 0  # The sum of tracked signal strengths

    for instruction in instructions:

        if instruction == 'noop':  # noop takes one cycle and has no effect
            cycles += 1
            register_change = 0  # Temporarily store for later
        else:  # addx V takes two cycles, and at the end of that cycle adjusts the register value
            cycles += 2
            register_change = int(instruction.split(' ')[1])  # Temporarily store for later

        # If we've reached, or passed, the cycle that we want to find the signal strength of,
        # then determine what the signal strength is and add it to the total sum
        if cycles >= cycle_check:
            print(f"Value at cycle {cycle_check}: {register}")
            signal_strength_sum += cycle_check * register

            cycle_check += 40

        # Adjust the register before moving to the next instruction
        register += register_change

    print(signal_strength_sum)


main()

end = timer()

print(f"Time elapsed: {end - start}")
