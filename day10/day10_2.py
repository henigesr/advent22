from timeit import default_timer as timer

# https://adventofcode.com/2022/day/10

start = timer()


def main():
    with open('input.txt', 'r') as f:
        instructions = f.read().splitlines()

    cycles = 0  # Tracks what cycle we're on
    register = 1  # The CPU register
    sprites = (0, 1, 2)  # Current sprites displaying
    line = ''  # The line being drawn

    for instruction in instructions:

        if instruction == 'noop':  # noop takes one cycle and has no effect
            cycles_todo = 1
            register_change = 0  # Temporarily store for later
        else:  # addx V takes two cycles, and at the end of that cycle adjusts the register value
            cycles_todo = 2
            register_change = int(instruction.split(' ')[1])  # Temporarily store for later

        for cycle in range(cycles_todo):
            if cycles % 40 == 0:  # At 40 pixels, print the line and start anew
                print(line)
                line = ''

            if cycles % 40 in sprites:  # If the sprite is present for the position we're on, draw it
                line += '@'
            else:  # Otherwise draw nothing
                line += ' '

            cycles += 1

        # Track our position and the 3 positions of the sprite
        register += register_change
        sprites = (register - 1, register, register + 1)

    print(line)


main()

end = timer()

print(f"Time elapsed: {end - start}")
