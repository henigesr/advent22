from timeit import default_timer as timer

# https://adventofcode.com/2022/day/8

start = timer()


def main():
    with open('input.txt', 'r') as f:
        tree_rows = f.read().splitlines()

    # Counter variable for visible trees
    vis_count = 0

    # tree_columns: list of strings, each string is a column read top to bottom
    tree_columns = []

    for i in range(0, len(tree_rows[0])):
        tree_column = ''.join([x[i] for x in tree_rows])  # Keeping as string to preserve leading 0's
        tree_columns.append(tree_column)

    # We'll loop through each tree one at a time, and see if any of the values to the left, right, top, or bottom
    # are less than it, or if it's on the edge.
    for tree_row_index, tree_row in enumerate(tree_rows):
        for column_index in range(0, len(tree_row)):

            # Find the max values to the left, right, top, or bottom of the current tree
            view_from_left = max([int(x) for x in tree_row[:column_index]], default=-1)
            view_from_right = max([int(x) for x in tree_row[column_index + 1:]], default=-1)
            view_from_above = max([int(x) for x in tree_columns[column_index][:tree_row_index]], default=-1)
            view_from_below = max([int(x) for x in tree_columns[column_index][tree_row_index + 1:]], default=-1)

            # If any of the max values in any of the four directions are less than the current tree's value, it is
            # visible, so we add it to the total count.
            if view_from_left < int(tree_row[column_index]) \
                    or view_from_right < int(tree_row[column_index]) \
                    or view_from_above < int(tree_row[column_index]) \
                    or view_from_below < int(tree_row[column_index]):
                vis_count += 1

    print(vis_count)


main()

end = timer()

print(f"Time elapsed: {end - start}")
