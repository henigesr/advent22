from timeit import default_timer as timer

# https://adventofcode.com/2022/day/8

start = timer()


def viewing_distance(tree_height, view):

    # Given the tree height as an integer, and the view as a list of integers starting adjacent to the tree,
    # find how many trees are visible in this direction.

    if not view:  # If looking out from the edge, there are no trees
        return 0

    for index, elem in enumerate(view):
        if elem >= tree_height:  # At the first tree in the view list that is the same or greater height
            return index + 1  # Return the number of trees we can see

    return len(view)  # If we can see all the way to the edge


def main():
    with open('input.txt', 'r') as f:
        tree_rows = f.read().splitlines()

    # For tracking the highest scenic score later on
    max_score = 0

    # tree_columns: list of strings, each string is a column read top to bottom
    tree_columns = []

    for i in range(0, len(tree_rows[0])):
        tree_column = ''.join([x[i] for x in tree_rows])  # Keeping as string to preserve leading 0's
        tree_columns.append(tree_column)

    # We'll loop through each tree one at a time, and see if any of the values to the left, right, top, or bottom
    # are less than it, or if it's on the edge.
    for tree_row_index, tree_row in enumerate(tree_rows):
        for column_index in range(0, len(tree_row)):

            # The scenic score for the current tree
            scenic_score = 0

            # A list of integers, starting from adjacent to the tree to the edge, for each of the 4 directions
            trees_left = [int(x) for x in tree_row[:column_index]]
            trees_left.reverse()  # Reversing to make first value adjacent to the tree
            trees_right = [int(x) for x in tree_row[column_index + 1:]]
            trees_above = [int(x) for x in tree_columns[column_index][:tree_row_index]]
            trees_above.reverse()  # Reversing to make first value adjacent to the tree
            trees_below = [int(x) for x in tree_columns[column_index][tree_row_index + 1:]]

            # Calculate the viewing distance in each direction
            r = viewing_distance(int(tree_row[column_index]), trees_right)
            l = viewing_distance(int(tree_row[column_index]), trees_left)
            a = viewing_distance(int(tree_row[column_index]), trees_above)
            d = viewing_distance(int(tree_row[column_index]), trees_below)

            # Calculate scenic score and determine if it's the highest so far
            scenic_score = r * l * a * d
            if scenic_score > max_score:
                max_score = scenic_score

    print(max_score)


main()

end = timer()

print(f"Time elapsed: {end - start}")
