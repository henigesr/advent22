from timeit import default_timer as timer
# https://adventofcode.com/2022/day/7

start = timer()


def main():
    with open('input.txt', 'r') as f:
        commands_list = f.read().splitlines()

    dir_sizes = {}

    pwd = ['/']

    for command in commands_list[1:]:  # Skip the first line since 'pwd' contains '/' already

        # For 'cd ..', remove last item (directory) from pwd, otherwise append the directory name
        if command[:4] == '$ cd':
            if command[5:] == '..':
                pwd.pop()
            else:
                pwd.append(command[5:] + '/')

        elif command[:4] != 'dir ' and command[:4] != '$ ls':  # We only care about lines with file sizes now
            file_size = int(command.split(' ')[0])

            # Loops through the pwd starting with '/' and adds the file size to each directory
            for i in range(1, len(pwd)+1):
                if ''.join(pwd[:i]) not in dir_sizes:
                    dir_sizes[''.join(pwd[:i])] = file_size
                else:
                    dir_sizes[''.join(pwd[:i])] += file_size

    # Find all of the directories with a total size of at most 100000, then calculate the sum of their total sizes.
    tot_sum = 0

    for key in dir_sizes:
        if dir_sizes[key] <= 100000:
            tot_sum += dir_sizes[key]

    print(tot_sum)


main()

end = timer()

print(f"Time elapsed: {end - start}")
