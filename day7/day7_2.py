from timeit import default_timer as timer
# https://adventofcode.com/2022/day/7

start = timer()


def main():
    with open('input.txt', 'r') as f:
        commands_list = f.read().splitlines()

    dir_sizes = {}

    pwd = ['/']

    for command in commands_list[1:]:  # Skip the first line since 'pwd' contains '/' already

        # For 'cd ..', remove last item (directory) from pwd, otherwise append the directory name
        if command[:4] == '$ cd':
            if command[5:] == '..':
                pwd.pop()
            else:
                pwd.append(command[5:] + '/')

        elif command[:4] != 'dir ' and command[:4] != '$ ls':  # We only care about lines with file sizes now
            file_size = int(command.split(' ')[0])

            # Loops through the pwd starting with '/' and adds the file size to each directory
            for i in range(1, len(pwd)+1):
                if ''.join(pwd[:i]) not in dir_sizes:
                    dir_sizes[''.join(pwd[:i])] = file_size
                else:
                    dir_sizes[''.join(pwd[:i])] += file_size

    # Calculate how much additional space is needed to be freed
    space_needed = 30000000 - (70000000 - dir_sizes['/'])

    smallest_dir = '/'  # Starting with the largest possible directory makes comparison easy

    # Loop through all directory sizes- if deleting the current directory frees enough space, and it's smaller than
    # the previous matching directory, store it in 'smallest_dir'
    for dir in dir_sizes:
        if space_needed < dir_sizes[dir] < dir_sizes[smallest_dir]:
            smallest_dir = dir

    print(dir_sizes[smallest_dir])


main()

end = timer()

print(f"Time elapsed: {end - start}")
